# DnD: A Transparent and Efficient Framework to Distribute Diversified Binaries based on Smart Contracts and Cryptography Accumulators

The diversification of software is meant to serve as a protective measure 
against exploitable flaws in binary files. The idea is to produce software 
binaries that are semantically the same but syntactically diverse. In particular,
compiler-based binary diversification provides an advantage due to its simplicity 
to implement. However, one key point of software diversity is to ensure that every 
user has a unique copy of the target application. This is challenging for 
popular software applications, which are downloaded and installed by 
millions of users every day. The present software supply chain, which is based
on the fingerprinting of a general copy for every user in order to ensure
effective distribution of verified software, is likewise incompatible with the
method of software diversification.

In this paper, we present a transparent and efficient framework to distribute 
diversified (thus the codename DnD) applications binaries. DnD is compatible 
with compiler-based software diversity, and all distribution process 
participants are held accountable. We propose two authentication strategies 
based on blockchain technology and cryptographic primitives that may be used to 
efficiently authenticate and validate a software binary during the installation 
process. Our empirical testings include calculating the computational cost in 
terms of time and storage. We also compute the transaction fees required to 
efficiently register optimized data onto a blockchain platform that serves as 
the underlying infrastructure. Our findings indicate that DnD is a viable 
method for distributing a large number of copies of a software application.

## Repository Structure
- [/cmd/diversify](/cmd/diversify) - scripts create a repository of N diversified binaries
- [/cmd/aptrsa](/cmd/aptrsa) - prototype and scripts create an accumulator using RSA 
- [/cmd/aptmerkle](/cmd/aptmerkle) - prototype and scripts create an accumulator using Merkle Trees 
- [/cmd/aptclient](/cmd/aptclient) - prototype and scripts to validate and install a software package 

## License
The DnD prototype and related artifacts are licensed under the terms of the [Apache 2.0](LICENSE).
