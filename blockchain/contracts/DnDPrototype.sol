// SPDX-License-Identifier: MIT
pragma solidity >=0.4.20 <0.6;

import "./lib/BigNumber.sol";

contract DnDPrototype {

    using BigNumber for *; 

    address owner;
    bytes32 merkle_root;
    bytes acc_rsa;
    uint acc_rsa_bitlen;
    bytes acc_N;
    uint acc_N_bitlen;

    constructor(address _owner) public {
        owner = _owner;
    }

    function commitRSA(bytes memory Q,
                       uint Q_bitlen,
                       bytes memory r,
                       uint r_bitlen,
                       bytes memory A,
                       uint A_bitlen,
                       uint nonce) public onlyOwner {
        if (acc_rsa.length == 0) {
            acc_rsa = A;
            acc_rsa_bitlen = A_bitlen;
            return;
        }

        BigNumber.instance memory bigN = BigNumber.instance(N, false, N_bitlen);
        BigNumber.instance memory bigg = BigNumber.instance(acc_rsa, false, acc_rsa_bitlen);
        BigNumber.instance memory bigr = BigNumber.instance(r, false, r_bitlen);
        BigNumber.instance memory bigQ = BigNumber.instance(Q, false, Q_bitlen);
        BigNumber.instance memory bigA = BigNumber.instance(A, false, A_bitlen);

        bytes memory l = hash_prime(abi.encodePacked(acc_rsa, A), nonce);
        uint l_bitlen = BigNumber.get_bit_length(l);
        BigNumber.instance memory bigl = BigNumber.instance(l, false, l_bitlen);

        BigNumber.instance memory Yp1 = bigQ.prepare_modexp(bigl, bigN);
        BigNumber.instance memory Yp2 = bigg.prepare_modexp(bigr, bigN);
        BigNumber.instance memory bigY = BigNumber.modmul(Yp1, Yp2, bigN);

        require(BigNumber.cmp(bigY, bigA, false) == 0);

        acc_rsa = A;
        acc_rsa_bitlen = A_bitlen;
    }

    function commitMerkle(bytes32 new_root, 
                    bytes32[] memory proofs, 
                    uint8[] memory sides) public onlyOwner {
        bytes32 hash_new;
        bytes32 hash_old;
        
        if (merkle_root == 0x00) {
            merkle_root = new_root;
            return;
        }
        
        if (proofs[0] == 0x00) {
            proofs[0] = merkle_root;
        }

        (hash_new, hash_old) = mth(proofs, sides, 0x00);
        require(hash_new == new_root);
        require(hash_old == merkle_root);

        merkle_root = hash_new;
    }
    
    function mth(bytes32[] memory proofs, 
                 uint8[] memory sides, 
                 bytes32 leaf) private pure returns(bytes32, bytes32) {
        uint i = 0;
        bytes32 hash_new = leaf;
        bytes32 hash_old = leaf;
        if (leaf == 0x00) {
            i = 1;
            hash_new = proofs[0];
            hash_old = proofs[0];
        }
        for (i; i < proofs.length; i++) {
            if (sides[i] == 1) {
                hash_new = sha256(abi.encodePacked(hash_new, proofs[i]));
            } else {
                hash_old = sha256(abi.encodePacked(proofs[i], hash_old));
                hash_new = sha256(abi.encodePacked(proofs[i], hash_new));
            }
        }
        return (hash_new, hash_old);
    }

    function hash_prime(bytes memory x, uint nonce) view private returns(bytes memory) {
        uint256 prime = uint256(sha256(x)) + uint256(nonce);
        uint256 entropy = uint256(blockhash(block.number - 1));
        assert(_is_prime(prime, 5, entropy));
        return abi.encodePacked(prime);
    }
    
    function _is_prime(uint256 n, uint32 k, uint256 entropy)
        private view returns (bool)
    {
        if(n == 2)
            return true;
        
        if( n < 2 || n % 2 == 0 )
            return false;
        
        uint256 d = n - 1;
        uint256 s = 0;
        
        while( d % 2 == 0 ) {
            d = d / 2;
            s += 1;
        }
        
        while( k-- != 0 ) {
            entropy = uint256(keccak256(abi.encodePacked(entropy, n))) % n;
            uint256 x = _modexp(entropy, d, n);
            if (x == 1 || x == n-1)
                continue;
            
            bool ok = false;
                
            for( uint j = 1; j < s; j++ ) {
                x = mulmod(x, x, n);
                if( x == 1 )
                    return false;
                if(x == n-1) {
                    ok = true;
                    break;
                }
            }
            if ( false == ok ) {
                return false;
            }
        }
        return true;
    }

    function _modexp(uint256 b, uint256 e, uint256 m)
        private view returns(uint256 result)
    {
        bool success;
        assembly {
            let freemem := mload(0x40)
            mstore(freemem, 0x20)
            mstore(add(freemem,0x20), 0x20)
            mstore(add(freemem,0x40), 0x20)
            mstore(add(freemem,0x60), b)
            mstore(add(freemem,0x80), e)
            mstore(add(freemem,0xA0), m)
            success := staticcall(39240, 5, freemem, 0xC0, freemem, 0x20)
            result := mload(freemem)
        }
        require(success);
    }

    function set_N(bytes memory _N, uint bitlen) public onlyOwner {
        acc_N = _N;
        acc_N_bitlen = bitlen;
    }

    function get_acc_N() view public returns (bytes memory) {
        return acc_N;
    }

    function get_acc_rsa() view public returns (bytes memory) {
        return acc_rsa;
    }

    function get_merkle_root() view public returns (bytes32) {
        return merkle_root;
    }

    modifier onlyOwner {
            require(
                msg.sender == owner,
                "Only owner can call this function."
            );
            _;
    }
}