package merkletree

import (
	"crypto/sha256"
	"math"
)

type MerkleTree struct {
	root      *Node
	aux       *Node
	last      *Node
	list      []*Node
	committed uint
}

func NewMerkleTree() *MerkleTree {
	return &MerkleTree{
		list: make([]*Node, 0),
	}
}

func (mt *MerkleTree) Add(h []byte) (bool, error) {
	node := NewNode(h)
	mt.list = append(mt.list, node)

	if mt.root == nil {
		mt.root = node
		mt.last = node
		mt.aux = node

		return true, nil
	}

	middle := NewEmptyNode()
	middle.Right = node
	middle.Side = RIGHT
	node.Parent = middle
	node.Side = RIGHT
	defer mt.updateBottomUp()

	if mt.root.LeftSize == mt.root.RightSize {
		middle.Left = mt.root

		mt.root.Side = LEFT
		mt.root.Parent = middle
		mt.root = middle
		mt.aux = middle

		return true, nil
	}

	if mt.aux.LeftSize == mt.aux.RightSize {
		middle.Parent = mt.aux.Parent
		middle.Left = mt.aux

		mt.aux.Parent.Right = middle
		mt.aux.Parent = middle
		mt.aux.Side = LEFT
		mt.aux = middle

		return true, nil
	}

	middle.Parent = mt.aux
	middle.Left = mt.aux.Right
	middle.Left.Side = LEFT

	mt.aux.Right.Parent = middle
	mt.aux.Right = middle
	mt.aux = middle

	return true, nil
}

func (mt *MerkleTree) Commit(m, first, last uint, b bool) ([][]byte, []uint) {
	n := last - first
	k := uint(math.Floor(math.Exp2(math.Ceil(math.Log2(float64(n)) - 1))))
	if m == n {
		if b {
			return [][]byte{[]uint8{0x00}}, []uint{0}
		} else {
			return [][]byte{mt.mth(first, last)}, []uint{0}
		}
	} else {
		if k < 1 {
			return [][]byte{}, []uint{}
		} else if m <= k {
			proofs, sides := mt.Commit(m, first, first+k, b)
			proofs = append(proofs, mt.mth(first+k, last))
			sides = append(sides, RIGHT)
			return proofs, sides
		} else {
			proofs, sides := mt.Commit(m-k, first+k, last, false)
			proofs = append(proofs, mt.mth(first, first+k))
			sides = append(sides, LEFT)
			return proofs, sides
		}
	}
}

func (mt *MerkleTree) mth(first, last uint) []byte {
	n := last - first
	k := uint(math.Ceil(math.Log2(float64(n))))
	p := mt.list[first]
	for k > 0 && p.Parent != nil {
		p = p.Parent
		k = k - 1
	}
	if p == nil {
		return nil
	}
	return p.Hash
}

func (mt *MerkleTree) updateBottomUp() {
	p := mt.aux
	for p != nil {
		h := sha256.Sum256(append(p.Left.Hash, p.Right.Hash...))
		p.Hash = h[:]

		p.LeftSize = p.Left.LeftSize + p.Left.RightSize + 1
		p.RightSize = p.Right.LeftSize + p.Right.RightSize + 1

		if p.LeftSize == p.RightSize {
			mt.aux = p
		}

		p = p.Parent
	}

}

func (mt *MerkleTree) Get(idx int) *Node {
	return mt.list[idx]
}

func (mt *MerkleTree) Len() int {
	return len(mt.list)
}

func (mt *MerkleTree) Root() *Node {
	return mt.root
}
