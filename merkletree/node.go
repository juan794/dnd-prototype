package merkletree

const (
	LEFT  = 0
	RIGHT = 1
)

type Node struct {
	Hash []byte
	Side uint

	Left, Right, Parent *Node
	LeftSize, RightSize uint64
}

func NewNode(h []byte) *Node {
	return &Node{
		Hash: h,
	}
}

func NewEmptyNode() *Node {
	return &Node{}
}

func (n *Node) Path() ([][]byte, []uint) {
	path := make([][]byte, 0)
	sides := make([]uint, 0)

	p := n
	for p.Parent != nil {
		if p.Side == RIGHT {
			path = append(path, p.Parent.Left.Hash)
			sides = append(sides, LEFT)
		} else {
			path = append(path, p.Parent.Right.Hash)
			sides = append(sides, RIGHT)
		}

		p = p.Parent
	}
	return path, sides
}
