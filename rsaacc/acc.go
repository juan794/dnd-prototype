package rsaacc

import (
	"crypto/rand"
	"crypto/sha256"
	"log"
	"math/big"
)

var (
	ONE = big.NewInt(1)
)

type Accumulator struct {
	accumulated *big.Int
	modulus     *big.Int
	previous    *big.Int

	p, q *big.Int
}

func NewRandomAccumulator(bits int) *Accumulator {
	p, err := rand.Prime(rand.Reader, bits/2)
	if err != nil {
		log.Fatal(err)
	}
	q, err := rand.Prime(rand.Reader, bits/2)
	if err != nil {
		log.Fatal(err)
	}

	N := p.Mul(p, q)
	g, err := rand.Int(rand.Reader, N)
	if err != nil {
		log.Fatal(err)
	}

	return &Accumulator{
		accumulated: g,
		modulus:     N,

		p: p,
		q: q,
	}
}

func NewAccumulator(p, q, g *big.Int) *Accumulator {
	N := p.Mul(p, q)

	return &Accumulator{
		accumulated: g,
		modulus:     N,

		p: p,
		q: q,
	}
}

func (this *Accumulator) AccumulateBatch(X []*big.Int) (*big.Int, []*big.Int) {
	proofs := this.RootFactor(this.accumulated, X)
	product := product(X)

	this.previous = this.accumulated
	this.accumulated = big.NewInt(0).Exp(this.accumulated, product, this.modulus)

	return product, proofs
}

func (this *Accumulator) NIPoE(product *big.Int) (*big.Int, *big.Int, uint) {
	concat := make([]byte, 0)
	concat = append(concat, this.previous.Bytes()...)
	concat = append(concat, this.accumulated.Bytes()...)
	h := sha256.Sum256(concat)

	l, nonce := HashToPrime(h[:])

	q := big.NewInt(0).Div(product, l)
	r := big.NewInt(0).Mod(product, l)
	Q := big.NewInt(0).Exp(this.previous, q, this.modulus)

	return r, Q, nonce

}

func (this *Accumulator) RootFactor(g *big.Int, X []*big.Int) []*big.Int {
	n := len(X)
	if n == 1 {
		return []*big.Int{g}
	}

	split := n / 2

	primesL := X[:split]
	primesR := X[split:]

	productL := product(primesL)
	productR := product(primesR)

	baseL := big.NewInt(0).Exp(g, productR, this.modulus)
	baseR := big.NewInt(0).Exp(g, productL, this.modulus)

	L := this.RootFactor(baseL, primesL)
	R := this.RootFactor(baseR, primesR)
	return append(L, R...)
}

func product(primes []*big.Int) *big.Int {
	product := big.NewInt(1)
	for _, v := range primes {
		product.Mul(product, v)
	}
	return product
}

func HashToPrime(h []byte) (*big.Int, uint) {
	// we receive hash of the file, coming from CSV file
	prime := big.NewInt(0).SetBytes(h[:])
	nonce := uint(0)
	for {
		if prime.ProbablyPrime(5) {
			return prime, nonce
		}
		prime.Add(prime, ONE)
		nonce++
	}
}

func (this *Accumulator) Modulus() *big.Int {
	return this.modulus
}

func (this *Accumulator) Previous() *big.Int {
	return this.previous
}

func (this *Accumulator) Accumulated() *big.Int {
	return big.NewInt(0).SetBytes(this.accumulated.Bytes())
}
