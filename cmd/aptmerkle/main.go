package main

import (
	"encoding/csv"
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/juan794/dnd-prototype/merkletree"
)

var (
	input   string
	output  string
	nameCol int
	hashCol int
)

func main() {
	flag.StringVar(&input, "input", "", "CSV file that contains previously calculate hashes from diversified packages, e.g. -input=FILE")
	flag.StringVar(&output, "output", "", "Directory to store proofs, e.g. -output=DIR")
	flag.IntVar(&nameCol, "name", 1, "Column in CSV file that contains package name, e.g. -name=NUMBER")
	flag.IntVar(&hashCol, "hash", 1, "Column in CSV file that contains package hash, e.g. -hash=NUMBER")
	flag.Parse()

	records, err := readInput()
	if err != nil {
		log.Fatal(err)
	}

	merkle := merkletree.NewMerkleTree()
	for _, record := range records {
		hash, err := hex.DecodeString(record[hashCol])
		if err != nil {
			log.Fatal("[hash]", err)
		}
		merkle.Add(hash)
	}

	proofs, sides := merkle.Commit(1, 0, uint(merkle.Len()), true)
	if err := writeAccumulator(merkle.Get(0).Hash, merkle.Root().Hash, proofs, sides); err != nil {
		log.Fatal("[hash]", err)
	}

	for idx := 0; idx < len(records); idx++ {
		proofs, sides := merkle.Get(idx).Path()
		if err := writeProof(records[idx][nameCol], proofs, sides); err != nil {
			log.Fatal("[output]", err)
		}
	}

}

func readInput() ([][]string, error) {
	file, err := os.Open(input)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	reader := csv.NewReader(file)
	records, err := reader.ReadAll()
	if err != nil {
		return nil, err
	}

	return records, nil
}

func writeAccumulator(base, root []byte, proofs [][]byte, sides []uint) error {
	file, err := os.Create(fmt.Sprintf("%s/base.acc", output))
	if err != nil {
		return err
	}
	defer file.Close()

	type MerkleJSON struct {
		Base   string   `json:"base"`
		Root   string   `json:"root"`
		Proofs []string `json:"path"`
		Sides  []uint   `json:"sides"`
	}

	_proofs := make([]string, len(proofs))
	for idx := range proofs {
		_proofs[idx] = hex.EncodeToString(proofs[idx])
	}

	obj := MerkleJSON{
		Base:   hex.EncodeToString(base),
		Root:   hex.EncodeToString(root),
		Proofs: _proofs,
		Sides:  sides,
	}

	data, err := json.MarshalIndent(obj, "", "\t")
	if err != nil {
		return err
	}
	file.Write(data)

	return nil
}

func writeProof(name string, proofs [][]byte, sides []uint) error {
	file, err := os.Create(fmt.Sprintf("%s/%s.acc", output, strings.TrimSuffix(name, filepath.Ext(name))))
	if err != nil {
		return err
	}
	defer file.Close()

	type ProofJSON struct {
		Proofs []string `json:"proofs"`
		Sides  []uint   `json:"sides"`
	}
	_proofs := make([]string, len(proofs))
	for idx := range proofs {
		_proofs[idx] = hex.EncodeToString(proofs[idx])
	}

	obj := ProofJSON{
		Proofs: _proofs,
		Sides:  sides,
	}

	data, err := json.MarshalIndent(obj, "", "\t")
	if err != nil {
		return err
	}
	file.Write(data)

	return nil
}
