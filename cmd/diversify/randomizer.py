#!/bin/python env
import os
import csv
import ntpath
import hashlib
import subprocess

from argparse import ArgumentParser

RANDOM_SIZE = 1024
STATS_HEADER = ['name', 'bytes', 'sha256']

def main():
    parser = ArgumentParser()
    parser.add_argument("-i", "--input", default="hugo_0.99.0.deb",
                        help=("input DEB file"))
    parser.add_argument("-o", "--output", default="/tmp/pool",
                        help=("output folder to store generated DEB files"))
    parser.add_argument("-n", "--size", default=1024, type=int,
                        help=("total number of diversifications"))
    parser.add_argument("-s", "--stats", default=None, action="store_true",
                        help=("Flag that indicates to calculate and store statistics"))
    parser.add_argument("-d", "--delete", default=None, action="store_true",
                        help=("Flag that indicates to delete generated files after created"))
                    
    args = parser.parse_args()
    
    if not os.path.exists('%s' % (args.input)):
        raise Exception("Input DEB file not found.")
    
    if not os.path.exists('%s' % (args.output)):
        raise Exception("Outout folder does not exist.")

    _, filename = ntpath.split(args.input)
    filename, extension = ntpath.splitext(filename)

    tempfolder = "/tmp/%s" % (filename, )
    diversefile = "%s/diverse.dat" % (tempfolder,)
    
    # DEB Unpacking - APT
    subprocess.call(["dpkg-deb", "-R", args.input, tempfolder])

    statsfile = None
    statswriter = None
    if args.stats:
        statsfile = open('stats_%s_%d.out' % (filename, args.size, ), 'a')
        statswriter = csv.writer(statsfile)


    for i in range(args.size):
        subprocess.call(["dd", "if=/dev/urandom", "of=%s" % (diversefile,), "bs=1", "count=%d" % (RANDOM_SIZE,)])
        h = get_file_hash(diversefile)

        outputfile = "%s/%s_%s%s" % (args.output, filename, h[-32:], extension)

        # DEB packing - APT
        subprocess.call(["dpkg-deb", "-b", tempfolder, outputfile])
    
        if args.stats:
            size = os.path.getsize(outputfile) 
            h = get_file_hash(outputfile)
            statswriter.writerow(["%s.%s" % (filename, extension, ), size, h])

        if args.delete:
            os.remove(outputfile)
    
    if args.stats:
        statsfile.close()

def get_file_hash(file):
    BUF_SIZE = 65536
    sha = hashlib.sha256()

    with open(file, 'rb') as f:
        while True:
            data = f.read(BUF_SIZE)
            if not data:
                break
            sha.update(data)
    return sha.hexdigest()

if __name__ == '__main__':
    main()