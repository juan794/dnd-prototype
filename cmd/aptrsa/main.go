package main

import (
	"encoding/csv"
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"math/big"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"gitlab.com/juan794/dnd-prototype/rsaacc"
)

const (
	BITS = 2048
)

var (
	p, _ = big.NewInt(0).SetString("ce30867d38be2793d18033e26694c1fbbcca9278f6a13d28850590fd75903921091c2abaca6253341fe327c49545f561f7a57949f5b736a09ed07b32ebc7a99b48ceaa55c070b4a1382b47696e3bf7d13e3f17371c15878f541e351c95bf10bd0cc365452a5ae0b6c4d10aa236eea4b2afefe64d70c13673ac8e1f022b398e628c3ea95d5d264de46d7be21e14c74f87b514c6b2fda277dfa73bb731201208781c61afb587149107ac5fdcc627fb18a2c1df8ac0b1ea99335a133f54e0de57bb9c134780f14a2a9a08661692625cd87020e806680a9f7ccfe5ec038162a303eedb87c259c9ab5d521e098b1b29ae656a0d784496046bac0a58e33abeb503c665", 16)
	q, _ = big.NewInt(0).SetString("f66ebfe9b6353977b9430f3b097e31c60726867cea45f44363e62b91be55900513cd5e174554a25e04e6f5ee52663e499408b7a378a1fe1919f68cd1015a9c930c20b608928e43ff7f3043489401267c0f27a81ac33412778a42f1450bd8f3b0f3cd14caedfcba3d8b72d903de9539bcc0a9bb24da6fe5a978cde0419ba3c91dc92ec8db2552dd4ccde7bd5332707ed038b043c69fcf0d4788fc0ff8b36aa2d35002e6e4858735582413bfd232de571256a9c70e7f95fefe504c1897184bf30373ee1d2c35a641204828179651fe632c4ebe9fb58d1d786e6b6a397c1144cfd8522d25598b58ed17d03553c5b5319f5b690a46a0d3dfc7a96347dc058df49eb9", 16)
	g, _ = big.NewInt(0).SetString("5bd342832841a424c8cff20f4fc7aabc34e2c15bf8ec0803be968aa469d81b57be6845641e35e37742874b34c1a9c07dd86061b90e70e73d1ef83c96cff82d7e5e6212c1645688c1f004feade9490c35ff105d11e779e98484dccaa89f96fe117d82c0d3c165deb5185c1705e97add09bc3faea50b24e405857d836a646f91f767232bdffe1ad13a0c107d931e7720c22807b5c5778639a94265ae278fd5bb9a626d315ab10848ead92dfbf8470da0b7bc85bbe93cca958293188fbf42792bed9ed79e69a91d8c642444c21071e72f005b330432de1880fbfb89473e36e7f9d682745ac1099f2934f6c6473fdb71557e81c5d5db7739c98945b23bffec4630ff9fbeec5ce001f490b9ccfb97fd2c9af7e529f0aad8342e03056689f745166597bfcda64c773062b6d6dcb3f046be7b837c59c8c238f596a8d5869b4f5efb2ca446eef19ba4d95063d3689df5dfa4273742cc569c650bbe323a40bfff48d097a5398b6a72687ee1edd8c5d9e494dc04e37b98301b61a973c326512d58292dbcf306411dc6389e28dc4ebcd1efee8e78e5cd3ecf6a574f6bc2deb60bb1f4cf3319ab477bda28ce1cd0e9571971370c7e980fcb691dc4dc08655e1ae2a421eaa4012f3d0c23da2cf08bf876f45c36f6ac7b382934e4c7f9f4380e32842450b507ee00e0592376a490b8901e4e132d18ae8239932dbde9b4b87652e44d0571d8b279", 16)
)

var (
	input   string
	output  string
	nameCol int
	hashCol int
)

func main() {
	flag.StringVar(&input, "input", "", "CSV file that contains previously calculate hashes from diversified packages, e.g. -input=FILE")
	flag.StringVar(&output, "output", "", "Directory to store proofs, e.g. -output=DIR")
	flag.IntVar(&nameCol, "name", 1, "Column in CSV file that contains package name, e.g. -name=NUMBER")
	flag.IntVar(&hashCol, "hash", 1, "Column in CSV file that contains package hash, e.g. -hash=NUMBER")
	flag.Parse()

	records, err := readInput(input)
	if err != nil {
		log.Fatal("[input]", err)
	}

	// Calculate Hash Prime
	nonces := make([]uint, len(records))
	primes := make([]*big.Int, len(records))
	for idx, record := range records {
		hash, err := hex.DecodeString(record[hashCol])
		if err != nil {
			log.Fatal("[hash]", err)
		}
		primes[idx], nonces[idx] = rsaacc.HashToPrime(hash)
	}

	acc := rsaacc.NewAccumulator(p, q, g)
	product, proofs := acc.AccumulateBatch(primes)
	r, Q, nonce := acc.NIPoE(product)

	if err := writeAccumulator(nonce, r, Q, acc.Previous(), acc.Accumulated(), acc.Modulus()); err != nil {
		log.Fatal("[output]", err)
	}

	for idx := 0; idx < len(records); idx++ {
		if err := writeProof(records[idx][nameCol], proofs[idx], nonces[idx]); err != nil {
			log.Fatal("[output]", err)
		}
	}

	if err := writeOutput(records, primes, nonces, proofs); err != nil {
		log.Fatal("[output]", err)
	}

	os.Exit(0)
}

func readInput(input string) ([][]string, error) {
	file, err := os.Open(input)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	reader := csv.NewReader(file)
	records, err := reader.ReadAll()
	if err != nil {
		return nil, err
	}

	return records, nil
}

func writeAccumulator(nonce uint, r, Q, g, A, N *big.Int) error {
	file, err := os.Create(fmt.Sprintf("%s/base.acc", output))
	if err != nil {
		return err
	}
	defer file.Close()

	type AccJSON struct {
		Nonce   int    `json:"nonce"`
		G       string `json:"g"`
		Gbitlen int    `json:"g_bitlen"`
		R       string `json:"r"`
		Rbitlen int    `json:"r_bitlen"`
		Q       string `json:"Q"`
		Qbitlen int    `json:"Q_bitlen"`
		A       string `json:"A"`
		Abitlen int    `json:"A_bitlen"`
		N       string `json:"N"`
		Nbitlen int    `json:"N_bitlen"`
	}

	obj := AccJSON{
		Nonce:   int(nonce),
		G:       hex.EncodeToString(g.Bytes()),
		Gbitlen: g.BitLen(),
		R:       hex.EncodeToString(r.Bytes()),
		Rbitlen: r.BitLen(),
		Q:       hex.EncodeToString(Q.Bytes()),
		Qbitlen: Q.BitLen(),
		A:       hex.EncodeToString(A.Bytes()),
		Abitlen: A.BitLen(),
		N:       hex.EncodeToString(N.Bytes()),
		Nbitlen: N.BitLen(),
	}

	data, err := json.MarshalIndent(obj, "", "\t")
	if err != nil {
		return err
	}
	file.Write(data)

	return nil
}

func writeProof(name string, proof *big.Int, nonce uint) error {
	file, err := os.Create(fmt.Sprintf("%s/%s.acc", output, strings.TrimSuffix(name, filepath.Ext(name))))
	if err != nil {
		return err
	}
	defer file.Close()

	type ProofJSON struct {
		Proof string `json:"proof"`
		Nonce uint   `json:"nonce"`
	}

	obj := ProofJSON{
		Proof: hex.EncodeToString(proof.Bytes()),
		Nonce: nonce,
	}

	data, err := json.MarshalIndent(obj, "", "\t")
	if err != nil {
		return err
	}
	file.Write(data)

	return nil
}

func writeOutput(records [][]string, primes []*big.Int, nonces []uint, proofs []*big.Int) error {
	file, err := os.Create(fmt.Sprintf("/tmp/%s", filepath.Base(input)))
	if err != nil {
		return err
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	for idx := 0; idx < len(records); idx++ {
		row := append(
			records[idx],
			[]string{
				hex.EncodeToString(primes[idx].Bytes()),
				strconv.Itoa(int(nonces[idx])),
				hex.EncodeToString(proofs[idx].Bytes()),
			}...,
		)

		if err := writer.Write(row); err != nil {
			return err
		}
	}

	return nil
}
