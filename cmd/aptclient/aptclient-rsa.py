#!/bin/python env
import json
import hashlib
import eth_utils as utils

from web3 import Web3
from argparse import ArgumentParser

RIGHT = 1
LEFT = 0

def main():
	parser = ArgumentParser()
	parser.add_argument("-pk", "--package", default="hugo_0.99.0.deb", help=("input DEB file")) 
	parser.add_argument("-p", "--proof", default="hugo_0.99.0.acc", help=("input proof file"))
	parser.add_argument("-ca", "--ca", default="0xE3d1982afE08cC231840D1787Eb0cAFC03909112", help=("CA address"))
	parser.add_argument("-nonce", "--nonce", default=2, type=int, help=("CA nonce"))
	args = parser.parse_args()
	
	with open(args.proof, 'r') as f:
		acc = json.load(f)

	h = get_file_hash(args.package)
	hashprime = hash_to_prime(h, acc['nonce'])
	base = int.from_bytes(bytes.fromhex(acc["proof"]), "big")
		
	contract_address = checksum_address_from(args.ca, args.nonce)

	w3 = Web3(Web3.HTTPProvider("http://localhost:8545"))
	contract = w3.eth.contract(address=contract_address, abi=ABI)
	on_contract = contract.functions.get_acc_rsa().call()
	on_contract_N = int.from_bytes(contract.functions.get_acc_N().call(), "big")
	

	check = pow(base, hashprime, on_contract_N)

    # If equals, proceeds to install software
	print(hex(check) == "0x" + on_contract.hex())

def hash_to_prime(x, nonce):
	prime = int.from_bytes(x, "big")
	prime = prime + nonce
	#assert(is_prime(prime))
	return prime

def is_prime(num):
    # Return True if num is a prime number. This function does a quicker
    # prime number check before calling rabin_miller().

    if (num < 2):
        return False # 0, 1, and negative numbers are not prime

    # About 1/3 of the time we can quickly determine if num is not prime
    # by dividing by the first few dozen prime numbers. This is quicker
    # than rabin_miller(), but unlike rabin_miller() is not guaranteed to
    # prove that a number is prime.
    lowPrimes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997]

    if num in lowPrimes:
        return True

    # See if any of the low prime numbers can divide num
    for prime in lowPrimes:
        if (num % prime == 0):
            return False


def checksum_address_from(_origin, _nonce):
    _origin = bytes.fromhex(_origin.replace('0x', ''))
    if _nonce == 0x00:
        data = b''.join([b'\xd6', b'\x94', _origin, b'\x80'])
    elif _nonce <= 0x7f:
        data = b''.join([b'\xd6', b'\x94', _origin, bytes([_nonce])])
    elif _nonce <= 0xff:
        data = b''.join([b'\xd7', b'\x94', _origin, b'\x81', bytes([_nonce])])
    elif _nonce <= 0xffff:
        data = b''.join([b'\xd8', b'\x94', _origin, b'\x82', bytes([_nonce])])
    elif _nonce <= 0xffffff:
        data = b''.join([b'\xd9', b'\x94', _origin, b'\x83', bytes([_nonce])])
    else:
        data = b''.join([b'\xda', b'\x94', _origin, b'\x84', bytes([_nonce])])
    addr = '0x' + utils.keccak(data)[-20:].hex()
    return utils.to_checksum_address(addr)


def get_file_hash(file):
    BUF_SIZE = 65536
    sha = hashlib.sha256()

    with open(file, 'rb') as f:
        while True:
            data = f.read(BUF_SIZE)
            if not data:
                break
            sha.update(data)
    return sha.digest()

ABI = '''[
	{
		"constant": false,
		"inputs": [
			{
				"internalType": "bytes32",
				"name": "new_root",
				"type": "bytes32"
			},
			{
				"internalType": "bytes32[]",
				"name": "proofs",
				"type": "bytes32[]"
			},
			{
				"internalType": "uint8[]",
				"name": "sides",
				"type": "uint8[]"
			}
		],
		"name": "commitMerkle",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"internalType": "bytes",
				"name": "Q",
				"type": "bytes"
			},
			{
				"internalType": "uint256",
				"name": "Q_bitlen",
				"type": "uint256"
			},
			{
				"internalType": "bytes",
				"name": "r",
				"type": "bytes"
			},
			{
				"internalType": "uint256",
				"name": "r_bitlen",
				"type": "uint256"
			},
			{
				"internalType": "bytes",
				"name": "A",
				"type": "bytes"
			},
			{
				"internalType": "uint256",
				"name": "A_bitlen",
				"type": "uint256"
			},
			{
				"internalType": "uint256",
				"name": "nonce",
				"type": "uint256"
			}
		],
		"name": "commitRSA",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"internalType": "bytes",
				"name": "_N",
				"type": "bytes"
			},
			{
				"internalType": "uint256",
				"name": "bitlen",
				"type": "uint256"
			}
		],
		"name": "set_N",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "_owner",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "get_acc_N",
		"outputs": [
			{
				"internalType": "bytes",
				"name": "",
				"type": "bytes"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "get_acc_rsa",
		"outputs": [
			{
				"internalType": "bytes",
				"name": "",
				"type": "bytes"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "get_merkle_root",
		"outputs": [
			{
				"internalType": "bytes32",
				"name": "",
				"type": "bytes32"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	}
]'''

if __name__ == '__main__':
    main()