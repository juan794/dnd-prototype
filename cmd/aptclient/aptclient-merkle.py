#!/bin/python env
import json
import hashlib
import eth_utils as utils

from web3 import Web3
from argparse import ArgumentParser

RIGHT = 1
LEFT = 0

def main():
    parser = ArgumentParser()
    parser.add_argument("-pk", "--package", default="hugo_0.99.0.deb", help=("input DEB file"))
    parser.add_argument("-p", "--proof", default="hugo_0.99.0.acc", help=("input proof file"))
    parser.add_argument("-ca", "--ca", default="0xE3d1982afE08cC231840D1787Eb0cAFC03909112", help=("CA address"))
    parser.add_argument("-nonce", "--nonce", default=0, type=int, help=("CA nonce"))
    args = parser.parse_args()

	#hash = get_file_hash(args.package)

    with open(args.proof, 'r') as f:
        acc = json.load(f)
	
    root = bytes.fromhex(acc['proofs'][0])
    for i in range(1, len(acc['proofs'])): 
        if acc['sides'][i] == RIGHT:
            root = hashlib.sha256(root + bytes.fromhex(acc['proofs'][i])).digest()
        else:
            root = hashlib.sha256(bytes.fromhex(acc['proofs'][i]) + root).digest()
    
    contract_address = checksum_address_from(args.ca, args.nonce)

    w3 = Web3(Web3.HTTPProvider("http://localhost:8545"))
    contract = w3.eth.contract(address=contract_address, abi=ABI)
    on_contract = contract.functions.get_merkle_root().call()

    # If equals, proceeds to install software
    print(on_contract.hex() == root.hex())

def checksum_address_from(_origin, _nonce):
    _origin = bytes.fromhex(_origin.replace('0x', ''))
    if _nonce == 0x00:
        data = b''.join([b'\xd6', b'\x94', _origin, b'\x80'])
    elif _nonce <= 0x7f:
        data = b''.join([b'\xd6', b'\x94', _origin, bytes([_nonce])])
    elif _nonce <= 0xff:
        data = b''.join([b'\xd7', b'\x94', _origin, b'\x81', bytes([_nonce])])
    elif _nonce <= 0xffff:
        data = b''.join([b'\xd8', b'\x94', _origin, b'\x82', bytes([_nonce])])
    elif _nonce <= 0xffffff:
        data = b''.join([b'\xd9', b'\x94', _origin, b'\x83', bytes([_nonce])])
    else:
        data = b''.join([b'\xda', b'\x94', _origin, b'\x84', bytes([_nonce])])
    addr = '0x' + utils.keccak(data)[-20:].hex()
    return utils.to_checksum_address(addr)


def get_file_hash(file):
    BUF_SIZE = 65536
    sha = hashlib.sha256()

    with open(file, 'rb') as f:
        while True:
            data = f.read(BUF_SIZE)
            if not data:
                break
            sha.update(data)
    return sha.digest()

ABI = '''[
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "_owner",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"constant": false,
		"inputs": [
			{
				"internalType": "bytes32",
				"name": "new_root",
				"type": "bytes32"
			},
			{
				"internalType": "bytes32[]",
				"name": "proofs",
				"type": "bytes32[]"
			},
			{
				"internalType": "uint8[]",
				"name": "sides",
				"type": "uint8[]"
			}
		],
		"name": "commitMerkle",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"internalType": "bytes",
				"name": "Q",
				"type": "bytes"
			},
			{
				"internalType": "uint256",
				"name": "Q_bitlen",
				"type": "uint256"
			},
			{
				"internalType": "bytes",
				"name": "r",
				"type": "bytes"
			},
			{
				"internalType": "uint256",
				"name": "r_bitlen",
				"type": "uint256"
			},
			{
				"internalType": "bytes",
				"name": "A",
				"type": "bytes"
			},
			{
				"internalType": "uint256",
				"name": "A_bitlen",
				"type": "uint256"
			},
			{
				"internalType": "bytes",
				"name": "N",
				"type": "bytes"
			},
			{
				"internalType": "uint256",
				"name": "N_bitlen",
				"type": "uint256"
			},
			{
				"internalType": "uint256",
				"name": "nonce",
				"type": "uint256"
			}
		],
		"name": "commitRSA",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "get_acc_rsa",
		"outputs": [
			{
				"internalType": "bytes",
				"name": "",
				"type": "bytes"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "get_merkle_root",
		"outputs": [
			{
				"internalType": "bytes32",
				"name": "",
				"type": "bytes32"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	}
]'''

if __name__ == '__main__':
    main()